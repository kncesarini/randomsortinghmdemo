package com.example;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "History")
public class History {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long Id;

	@Column
	private String old_numbers;
	
	@Column
	private String sorted_numbers;
	
	@Column
	private long swaps;

	@Column
	private long timer;

	public History() {
		
	}
	


	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getOld_numbers() {
		return old_numbers;
	}


	public void setOld_numbers(String old_numbers) {
		this.old_numbers = old_numbers;
	}

	public String getSorted_numbers() {
		return sorted_numbers;
	}

	public void setSorted_numbers(String sorted_numbers) {
		this.sorted_numbers = sorted_numbers;
	}

	public long getSwaps() {
		return swaps;
	}
	public void setSwaps(long swaps) {
		this.swaps = swaps;
	}

	public long getTimer() {
		return timer;
	}

	public void setTimer(long timer) {
		this.timer = timer;
	}
	
}
