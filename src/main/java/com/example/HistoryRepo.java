package com.example;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository
public class HistoryRepo {

	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Add History object to persistent storage
	 * @param data
	 */
	public void add(History data)  {
		
			entityManager.persist(data);
			entityManager.flush();
		
	}

	/**
	 * remove all History objects from persistent storage
	 */
	public void removeAll() {
		
		entityManager.createQuery("delete from History").executeUpdate();
		//	TODO: lets see later
	}

	/**
	 * get all history objects in persistent storage
	 * @return List<History>
	 */
	@SuppressWarnings("unchecked")
	public List<History> getAll() {
		String sql = "from History";
			
		List<History> result =  (List<History>) entityManager.createQuery(sql).getResultList();
		
		return result;
	}
	
	

}
