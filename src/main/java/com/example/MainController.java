package com.example;


import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class MainController {

	@Autowired
	HistoryRepo repo;

	
	
	/**
	 * Remove everything from the database table
	 * @param model
	 * @return view
	 */
    @GetMapping("/reset")
    public String resetNumbers(Model model) {
       
    	repo.removeAll(); 	
    	
        return "redirect:/";
    }
    
    
    /**
     * default handler to display history list
     * @param model
     * @return view
     */
    @GetMapping("/")
    public String newNumbers(Model model) {

    	
    	model.addAttribute("history", repo.getAll());
    	
    	return "numbers";
    }
    
    /**
     * handle adding a new batch of numbers
     * @param paramCount form parameter- how many random numbers to generate
     * @param model
     * @return view
     */
    @PostMapping("/newnumbers")
    public String newNumbers(@RequestParam("count") int paramCount) {
    	
    	int count = Math.max(0, paramCount); //avoid negative counts
    	
    	Random random = new Random();
    	Numbers newNumbers = new Numbers();

    	//Generate some random numbers
    	for (int i = 0; i < count; i++) {
    		newNumbers.add(random.nextInt(100));
    	}

    	String fromNumbers = newNumbers.toString();
    	
    	//start time-taking
    	long timer = System.currentTimeMillis();
    	
    	//sort the generated list
    	int swaps = newNumbers.randomSort();
    	
    	// end time-taking
    	timer = System.currentTimeMillis()-timer;
    	
    	History history = new History();
    	
    	history.setOld_numbers(fromNumbers);
    	history.setSorted_numbers(newNumbers.toString());
    	history.setSwaps(swaps);
    	history.setTimer(timer);

    	//persist to database
    	repo.add(history);
    	
    	
    	
    	//redirect to / which will display the results
        return "redirect:/";
    }
}
