package com.example;

import java.util.ArrayList;
import java.util.Random;

/*
 * Simple class to enable a bad sorting algorithm to be used on an arraylist of numbers
 */

public class Numbers extends ArrayList<Integer> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1863466192165382180L;

	/**
	 * checks if the arraylist is orderered (ascending)
	 * @return true if ordered
	 */
	public boolean isOrdered() {
		if (this.size() > 1) {
			int x = 0;
			while (x < this.size()-1) {
				if (this.get(x).intValue() > this.get(x+1).intValue())
					return false;
				x++;
			}
		}
		return true;
	}
	
	/**
	 * swaps two random elements in the arraylist
	 */
	public void swapRandomly() {
		if (this.size() > 1) {

			Random random = new Random();
			int x = random.nextInt(this.size());
			int y;
			do {
				y = random.nextInt(this.size());
			} while (y == x);

			Integer temp = this.get(x);
			this.set(x, this.get(y));

			this.set(y, temp);

		}
	}
	/**
	 * Sort the entire arraylist in ascending order
	 * @return number of pair-wise swaps performed in order to sort the list
	 */
	public int randomSort(){
		int swaps = 0;
		while(!this.isOrdered()) {
    		++swaps;
    		this.swapRandomly();
    	}
		return swaps;
	}
	/**
	 * removes [ and ] from default implementation for slightly prettier output
	 */
	@Override
	public String toString(){
		String temp = super.toString();
		temp = temp.substring(1, temp.length()-1);
		
		return temp;
	}
}
