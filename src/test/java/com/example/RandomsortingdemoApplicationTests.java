package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RandomsortingdemoApplicationTests {

	@Autowired
	private HistoryRepo repo;
	
	@Test
	public void testSort() {
		Numbers numbers = new Numbers();
		numbers.add(9);
		numbers.add(4);
		numbers.add(3);
		numbers.add(65);
		numbers.add(1);
		Assert.assertEquals(numbers.size(), 5);
		numbers.randomSort();
		Assert.assertEquals(numbers.get(0).intValue(), 1);
		Assert.assertEquals(numbers.get(1).intValue(),3);
		Assert.assertEquals(numbers.get(2).intValue(), 4);
		Assert.assertEquals(numbers.get(3).intValue(), 9);
		Assert.assertEquals(numbers.get(4).intValue(), 65);

	}
	
	@Test
	public void testAddHistory() {
		
		Assert.assertTrue(repo.getAll().size() == 0);
		
		String oldNumbers = "2, 3, 4, 1";
		String sortedNumbers = "1, 2, 3, 4";
		
		History history = new History();
		history.setOld_numbers(oldNumbers);
		history.setSorted_numbers(sortedNumbers);
		history.setSwaps(55);
		history.setTimer(5);
		
		repo.add(history);
		
		Assert.assertEquals(repo.getAll().size(), 1);
		
		History h2 = repo.getAll().get(0);
		
		Assert.assertTrue(h2.getOld_numbers().equals(oldNumbers));
		Assert.assertTrue(h2.getSorted_numbers().equals(sortedNumbers));
		Assert.assertEquals(h2.getSwaps(), 55);
		Assert.assertEquals(h2.getTimer(), 5);
	}
	@Test
	public void testResetHistory() {
		
		Assert.assertEquals(repo.getAll().size(), 0);
		
		String oldNumbers = "2, 3, 4, 1";
		String sortedNumbers = "1, 2, 3, 4";
		
		History history = new History();
		history.setOld_numbers(oldNumbers);
		history.setSorted_numbers(sortedNumbers);
		history.setSwaps(55);
		history.setTimer(5);
		
		repo.add(history);
		
		Assert.assertEquals(repo.getAll().size(), 1);
		
		repo.removeAll();
		
		
		Assert.assertEquals(repo.getAll().size(), 0);
		
	}

}
